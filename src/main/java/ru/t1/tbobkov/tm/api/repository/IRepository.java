package ru.t1.tbobkov.tm.api.repository;

import ru.t1.tbobkov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public interface IRepository<M extends AbstractModel> {

    default IRepositoryOptional<M> optional() {
        return new IRepositoryOptional<M>() {
            @Override
            public Optional<M> findOneById(String id) {
                return Optional.ofNullable(IRepository.this.findOneById(id));
            }

            @Override
            public Optional<M> findOneByIndex(Integer index) {
                return Optional.ofNullable(IRepository.this.findOneByIndex(index));
            }
        };
    }

    interface IRepositoryOptional<M> {

        Optional<M> findOneById(String id);

        Optional<M> findOneByIndex(Integer index);

    }

    void clear();

    List<M> findAll();

    List<M> findAll(final Comparator<M> comparator);

    M add(final M model);

    boolean existsById(String id);

    M findOneById(String id);

    M findOneByIndex(Integer index);

    int getSize();

    M remove(M model);

    M removeById(String id);

    M removeByIndex(Integer index);

    void removeAll(Collection<M> collection);

}
