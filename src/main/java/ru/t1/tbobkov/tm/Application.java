package ru.t1.tbobkov.tm;

import ru.t1.tbobkov.tm.component.Bootstrap;

public final class Application {

    public static void main(String... args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}