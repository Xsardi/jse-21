package ru.t1.tbobkov.tm.command.project;

import ru.t1.tbobkov.tm.model.Project;
import ru.t1.tbobkov.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    private static final String NAME = "project-remove-by-id";

    private static final String DESCRIPTION = "find project by id and remove it from storage";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        if (project == null) return;
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}