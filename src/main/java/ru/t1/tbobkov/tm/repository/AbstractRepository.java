package ru.t1.tbobkov.tm.repository;

import ru.t1.tbobkov.tm.api.repository.IRepository;
import ru.t1.tbobkov.tm.model.AbstractModel;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    private final Map<String, M> models = new LinkedHashMap<>();

    @Override
    public void clear() {
        models.clear();
    }

    @Override
    public List<M> findAll() {
        return new ArrayList<>(models.values());
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @Override
    public M add(final M model) {
        models.put(model.getId(), model);
        return model;
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public M findOneById(final String id) {
        return models.get(id);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return models.get(index);
    }

    @Override
    public int getSize() {
        return models.size();
    }

    @Override
    public M remove(final M model) {
        if (model == null) return null;
        models.remove(model.getId());
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        return remove(model);
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        return remove(model);
    }

    @Override
    public void removeAll(final Collection<M> collection) {
        collection
                .stream()
                .map(AbstractModel::getId)
                .forEach(models::remove);
    }

}
